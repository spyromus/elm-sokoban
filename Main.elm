import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (on, keyCode, onClick)
import Exts.Html exposing (nbsp)
import Debug exposing (log)
import Json.Decode as Json
import Array exposing (..)
import Tuple exposing (first, second)
import Keyboard exposing (KeyCode)

type alias Row = String
type alias Map = List Row
type alias Position = (Int, Int)
type alias Model =
  { map    : Map
  , crates : List Position
  , docks  : List Position
  , player : Position
  }

type Msg
  = KeyDown KeyCode
  | NoOp

-- Game map
initialMap : Map
initialMap =
  [ "    XXXXX             "
  , "    X   X             "
  , "    X   X             "
  , "  XXX   XXX           "
  , "  X       X           "
  , "XXX X XXX X     XXXXXX"
  , "X   X XXX XXXXXXX    X"
  , "X                    X"
  , "XXXXX XXXX X XXXX    X"
  , "    X      XXX  XXXXXX"
  , "    XXXXXXXX          "
  ]

-- Function that is supposed to read the map and extract crates, docks and player positions
-- from it. For now it has them hard coded.
initMap : Map -> Model
initMap map =
    { map    = map
    , crates = [ (5, 2), (7, 3), (5, 4), (8, 4), (2, 7), (5, 7) ]
    , docks  = [ (19, 6), (20, 6), (19, 7), (20, 7), (19, 8), (20, 8) ]
    , player = (12, 8)
    }

-- Returns the game symbol for the certain offset from the current player position:
-- ' ' - for empty space
-- '#' - for wall
-- '*' - for crate
cell : Model -> Int -> Int -> Char
cell model dx dy =
  let
      px = first model.player
      py = second model.player
  in
    if List.member (px + dx, py + dy) model.crates then
       '*'
    else
      case (Array.get (py + dy) (Array.fromList model.map)) of
        Nothing  -> '#'
        Just row ->
          case Array.get (px + dx) (Array.fromList (String.toList row)) of
            Nothing   -> '#'
            Just cell -> cell

-- Checks if the player can move to a certain offset from its current position
canMove : Model -> Int -> Int -> Bool
canMove model dx dy = (cell model dx dy) == ' '

-- Checks if there is a crate
hasCrate : Model -> Int -> Int -> Bool
hasCrate model dx dy = (cell model dx dy) == '*'

-- Shifts the crate in a certain direction
shiftCrate : Int -> Int -> Model -> Model
shiftCrate dx dy model =
  let
    px        = first model.player                                  -- player coords
    py        = second model.player
    p1        = (px + dx, py + dy)                                  -- coords of the cell the player is moving to
    p2        = (px + 2 * dx, py + 2 * dy)                          -- coords of the cell behind that cell
    newCrates = p2 :: (List.filter (\p -> p /= p1) model.crates)    -- remove the old crate coords and append new
  in
    if (hasCrate model dx dy) && (canMove model (dx * 2) (dy * 2)) then
      { model | crates = newCrates }
    else
      model

-- Moves the player in a certain direction
movePlayer : Int -> Int -> Model -> Model
movePlayer dx dy model =
  let
    px        = first model.player                                  -- player coords
    py        = second model.player
  in
    if canMove model dx dy then
      { model | player = (px + dx, py + dy) }
    else
      model

-- Checks if a shift of a crate or a move are available and performs the action
checkAndMove : Model -> Int -> Int -> Model
checkAndMove model dx dy =
  model |> (shiftCrate dx dy) |> (movePlayer dx dy)

-- View update
update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    KeyDown code -> (handleKeyPress model code, Cmd.none)
    _            -> (model, Cmd.none)

-- Handles user key press. We support only arrow keys for now
handleKeyPress : Model -> KeyCode -> Model
handleKeyPress model code =
  case code of
    37 -> checkAndMove model -1  0
    39 -> checkAndMove model  1  0
    38 -> checkAndMove model  0 -1
    40 -> checkAndMove model  0  1
    _  -> model

-- View rendering
view : Model -> Html Msg
view model =
  div [ ]
    [ node "style" [ type_ "text/css" ] [ text styles ]
    , div [ class "map" ] (List.indexedMap (\rowY row -> mapViewRow rowY row model) model.map)
    , (scoreView model)
    ]

-- Player score view
scoreView : Model -> Html Msg
scoreView model =
  div [ class "map-scores" ]
    [ text "Контейнеры на месте:"
    , div [ class "map-scores-counter" ]
      [ text (toString (List.length (List.filter (\c -> List.member c model.docks) model.crates))) ]
    ]

-- Map rendering
mapViewRow : Int -> Row -> Model -> Html Msg
mapViewRow rowY row model =
  div [ class "map-row" ] (List.indexedMap (\rowX col -> mapViewCol rowX rowY col model) (String.toList row))

mapViewCol : Int -> Int -> Char -> Model -> Html Msg
mapViewCol rowX rowY col model =
  span [ class "map-cell" ] [ text (mapViewCell model rowX rowY col) ]

mapViewCell : Model -> Int -> Int -> Char -> String
mapViewCell model rowX rowY col =
  let
    p = (rowX, rowY)
  in
    if model.player == p then
      "@"
    else if List.member p model.crates then
      "*"
    else if List.member p model.docks then
      "."
    else
      case col of
        ' ' -> nbsp
        _   -> String.fromChar(col)

-- Program initialization
init : (Model, Cmd Msg)
init = ((initMap initialMap), Cmd.none)

-- Main function
main =
  Html.program
    { init = init
    , view  = view
    , update = update
    , subscriptions = subscriptions
    }


styles : String
styles =
  """
  html, head, body {
    height: auto;
  }

  body {
    display: flex;
    justify-content: space-around;
  }

  .map {
    margin: 100px 0;
  }

  .map-cell {
    display: inline-block;
    width: 20px;
    height: 20px;
  }

  .map-scores {
    font-size: 32px;
    margin: 20px;
    text-align: center;
    color: #888;
  }
  .map-scores-counter {
    font-size: 48px;
    margin: 20px;
    color: #444;
  }
  """

subscriptions : Model -> Sub Msg
subscriptions model = Keyboard.downs KeyDown
